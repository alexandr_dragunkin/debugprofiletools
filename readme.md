# Профиллировщик DebugProfileTools #

*Author:*  Aleksandr Dragunkin --<alexandr69@gmail.com>

*Created:* 25.12.2018

**Назначение:**  Сбор характеристик работы программы, таких как время выполнения отдельных фрагментов (обычно  подпрограмм), число верно предсказанных условных переходов, число кэш-промахов и т. д.

**Результат:** 

-   текстовый файл-отчет с укзанием адреса функции и модуля  с его числом вызовов и затраченном времени на вызов и их суммарное время.
 
-   Картинку PNG с графом вызовов функций во время выполнения.

**Требования:** 

- интерпретатор Python 3-й версии

- [Graphviz](http://www.graphviz.org/) желательно (2.38)

- gprof2dot Если вызывает вопрос что это, то берем его [здесь](https://pypi.org/project/gprof2dot/)

Пакет содержит  файл `DebugProfileTools\dpt_config.ini`, в котором хранятся пути к установленному *Graphviz* и адресом для папки *PROFILE*

> **DebugProfileTools\dpt_config.ini**
```
[Graphviz]
path = c:\Program Files (x86)\Graphviz2.38\bin
[APPDATA]
path = c:\PKM74\Bin\APP
```

## Пример ##


```python
from DebugProfileTools import (profile)

....

какой то код

....


@profile
def Create():
   ....
   профилируемая функция. Обычно это main
   ....
```

Ниже представлены результаты профилирования модуля CreateBase.

Каждый следующий вызов не затирает предыдущие при совпадении имен, а создает файл с индексом в скобках.

![](https://habrastorage.org/webt/lc/15/pe/lc15pegf9q6drhiaof3kkhzj-a4.png)

`*.prof` файл результата работы профилировщика cProfile
`*.res`  граф-задание для чтения утилитой Graphviz подготовленый gprof2dot
`*.png`  граф-картинка вызовов  функций 
`*.txt`  отчет в текстовой форме

![](https://habrastorage.org/webt/bl/ia/1o/blia1occ9xeoocy6_shwx-itonk.png)